# Debian packages for cri-tools

This repository contains build configuration to create Debian packages from
cri-tools official release binaries.

## Packages

### `crictl`

Contains the `crictl` binary.

## Building

Install build dependencies (with [mk-build-deps][1] for instance) then run
`debuild -b -us -uc`.

Packages should build on any x86_64 distribution with debhelper >=12 available.

[1]: https://manpages.debian.org/buster/devscripts/mk-build-deps.1.en.htm

## Checking for new upstream versions

Information about new Kubernetes releases can be found in the CHANGELOG files
in their git repository. `uscan` and the `debian/watch` file allow for new
minor releases discovery:

```
uscan --no-download
```

## Contributing

Every contributions are welcome.

Participation in this project is subject to a [code of
conduct](CODE_OF_CONDUCT.md).
