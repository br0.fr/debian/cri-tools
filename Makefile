INSTALL = install

VERSION = 1.26.1
ARCHIVE_CHECKSUM = 0c1a0f9900c15ee7a55e757bcdc220faca5dd2e1cfc120459ad1f04f08598127
ARCHIVE_NAME = crictl-v${VERSION}-linux-amd64.tar.gz
ARCHIVE_URL = https://github.com/kubernetes-sigs/cri-tools/releases/download/v${VERSION}/${ARCHIVE_NAME}

build:
	mkdir -p build/
	curl -s -L ${ARCHIVE_URL} -C - -o build/${ARCHIVE_NAME}
	echo "${ARCHIVE_CHECKSUM}  build/${ARCHIVE_NAME}" | sha256sum --quiet -c -
	tar -x -f build/${ARCHIVE_NAME} -C build/

install:
	${INSTALL} -d ${DESTDIR}/usr/bin
	${INSTALL} -m 755 build/crictl ${DESTDIR}/usr/bin

clean:
	rm -rf build/
